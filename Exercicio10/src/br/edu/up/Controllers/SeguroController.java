import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SeguroController {
    private List<Seguro> seguros;

    public SeguroController() {
        this.seguros = new ArrayList<>();
    }

    public void incluirSeguro(Seguro seguro) {
        seguros.add(seguro);
    }

    public Seguro localizarSeguro(String numeroApolice) {
        for (Seguro seguro : seguros) {
            if (seguro.getApolice().equals(numeroApolice)) {
                return seguro;
            }
        }
        return null;
    }

    public boolean excluirSeguro(String numeroApolice) {
        Seguro seguro = localizarSeguro(numeroApolice);
        if (seguro != null) {
            seguros.remove(seguro);
            return true;
        }
        return false;
    }

    public void excluirTodosSeguros() {
        seguros.clear();
    }

    public List<Seguro> listarTodosSeguros() {
        return seguros;
    }

    public int quantidadeSeguros() {
        return seguros.size();
    }

    public void adicionarSeguroVida(String numeroApolice, Segurado segurado, BigDecimal valorApolice, LocalDate inicio, LocalDate fim, boolean cobreDoenca, boolean cobreAcidente) {
        SeguroVida seguroVida = new SeguroVida(numeroApolice, segurado, valorApolice, inicio, fim, cobreDoenca, cobreAcidente);
        incluirSeguro(seguroVida);
    }

    public void adicionarSeguroVeiculo(String numeroApolice, Segurado segurado, BigDecimal valorApolice, LocalDate inicio, LocalDate fim, double valorFranquia, boolean carroReserva, boolean cofresVidros) {
        SeguroVeiculo seguroVeiculo = new SeguroVeiculo(numeroApolice, segurado, valorApolice, inicio, fim, valorFranquia, carroReserva, cofresVidros);
        incluirSeguro(seguroVeiculo);
    }
}
