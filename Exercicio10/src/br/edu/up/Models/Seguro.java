package br.edu.up;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class Seguro {
    private String apolice;
    private Segurado segurado;
    private BigDecimal vlrApolice;
    private LocalDate dtInicio;
    private LocalDate dtFim;

    public String getApolice() {
        return apolice;
    }

    public void setApolice(String apolice) {
        this.apolice = apolice;
    }

    public Segurado getSegurado() {
        return segurado;
    }

    public void setSegurado(Segurado segurado) {
        this.segurado = segurado;
    }

    public BigDecimal getVlrApolice() {
        return vlrApolice;
    }

    public void setVlrApolice(BigDecimal vlrApolice) {
        this.vlrApolice = vlrApolice;
    }

    public LocalDate getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(LocalDate dtInicio) {
        this.dtInicio = dtInicio;
    }

    public LocalDate getDtFim() {
        return dtFim;
    }

    public void setDtFim(LocalDate dtFim) {
        this.dtFim = dtFim;
    }
}

