package br.edu.up;

public class SeguroVida extends Seguro {
    private boolean cobreDoenca;
    private boolean cobreAcidente;

    public SeguroVida(String apolice, Segurado segurado, BigDecimal vlrApolice, LocalDate dtInicio, LocalDate dtFim, boolean cobreDoenca, boolean cobreAcidente) {
        super(apolice, segurado, vlrApolice, dtInicio, dtFim);
        this.cobreDoenca = cobreDoenca;
        this.cobreAcidente = cobreAcidente;
    }

    public boolean isCobreDoenca() {
        return cobreDoenca;
    }

    public void setCobreDoenca(boolean cobreDoenca) {
        this.cobreDoenca = cobreDoenca;
    }

    public boolean isCobreAcidente() {
        return cobreAcidente;
    }

    public void setCobreAcidente(boolean cobreAcidente) {
        this.cobreAcidente = cobreAcidente;
    }

    public String getDados() {
        return "SeguroVida{" +
                "apolice='" + getApolice() + '\'' +
                ", segurado=" + getSegurado() +
                ", vlrApolice=" + getVlrApolice() +
                ", dtInicio=" + getDtInicio() +
                ", dtFim=" + getDtFim() +
                ", cobreDoenca=" + cobreDoenca +
                ", cobreAcidente=" + cobreAcidente +
                '}';
    }
}

