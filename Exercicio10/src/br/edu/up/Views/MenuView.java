package br.edu.up.Views;

public class MenuView {

    public SeguroView seguroView = new SeguroView();

    public void exibirMenu() {
        int opcao;
        do {
            System.out.println("==== Menu de Seguros ====");
            System.out.println("1. Incluir Seguro");
            System.out.println("2. Localizar Seguro");
            System.out.println("3. Excluir Seguro");
            System.out.println("4. Excluir Todos os Seguros");
            System.out.println("5. Listar Todos os Seguros");
            System.out.println("6. Ver Quantidade de Seguros");
            System.out.println("7. Sair");
            System.out.print("Escolha uma opção: ");
            opcao = scanner.nextInt();
            scanner.nextLine(); 

            switch (opcao) {
                case 1:
                seguroView.incluirSeguro();
                    break;
                case 2:
                seguroView.localizarSeguro();
                    break;
                case 3:
                seguroView.excluirSeguro();
                    break;
                case 4:
                seguroView.excluirTodosSeguros();
                    break;
                case 5:
                seguroView.listarTodosSeguros();
                    break;
                case 6:
                seguroView.verQuantidadeSeguros();
                    break;
                case 7:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        } while (opcao != 7);
    }

}
