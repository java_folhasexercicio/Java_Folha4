package br.edu.up.Modelo;

public class compromisso {
    private String Pessoa;
    private String Local;
    private String Assunto;
    private int Hora;

    public compromisso(String pessoa, String local, String assunto, int hora) {
        this.Pessoa = pessoa;
        this.Local = local;
        this.Assunto = assunto;
        this.Hora = hora;
    }

    public void setPessoa(String pessoa) {
        Pessoa = pessoa;
    }

    public void setLocal(String local) {
        Local = local;
    }

    public void setAssunto(String assunto) {
        Assunto = assunto;
    }

    public void setHora(int hora) {
        Hora = hora;
    }

    public String getPessoa() {
        return this.Pessoa;
    }

    public String getLocal() {
        return this.Local;
    }

    public String getAssunto() {
        return this.Assunto;
    }

    public int getHora() {
        return this.Hora;
    }

}
