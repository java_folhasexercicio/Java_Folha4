package br.edu.up.Modelo;

public class mes {
    private String Nome;
    private int qntdDeDias;
    private int Dias;

    public mes(String Nome, int qntdDeDias, int Dias) {
        this.Nome = Nome;
        this.qntdDeDias = qntdDeDias;
        this.Dias = Dias;
    }

    public void adicionarCompromisso(compromisso Compromisso, int diaMes) {

        dia Dia = new dia(diaMes);

        Dia.adicionarCompromisso(Compromisso);
    }

    public void adicionarCompromisso(String Pessoa, String local, String Assunto, int Hora, int DiaMes) {

        dia Dia = new dia(DiaMes);

        compromisso Compromisso = new compromisso(Pessoa, local, Assunto, Hora);

        Dia.adicionarCompromisso(Compromisso);

    }
}
