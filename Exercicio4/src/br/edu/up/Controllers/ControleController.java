package br.edu.up.Controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import br.edu.up.Models.*;
import br.edu.up.Models.Movimentacao.Tipo;

public class ControleController {

    Estacionamento estacionamento = new Estacionamento();

    public Boolean RegistraEntrada(Veiculo veiculo) throws Exception {

        try {
            var qtdVagas = estacionamento.ConsultaQtdVagas();

            if (qtdVagas > 0) {
                if (veiculo != null) {
                    estacionamento.NovaEntrada(veiculo, LocalDateTime.now());
                    return true;

                } else {
                    throw new Exception("Para registrar uma ENTRADA, um veículo deve ser informado");
                }
            } else {
                throw new Exception("Não há mais vagas disponíveis");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Boolean RegistraSaida(Veiculo veiculo) throws Exception {

        try {
            if (veiculo != null) {
                estacionamento.NovaSaida(veiculo, LocalDateTime.now());
                return true;

            } else {
                throw new Exception("Para registrar uma SAÍDA, um veículo deve ser informado");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Boolean ConsultaVeiculo(String placa) throws Exception {

        try {
            if (placa != "") {
                return estacionamento.ConsultaPlaca(placa);
            } else {
                throw new Exception("Para registrar uma saída, um veículo deve ser informado");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void RelatorioPorPeriodoETipo(String periodo, Movimentacao.Tipo tipo) {
        LocalDateTime periodoInicio = LocalDateTime.now();
        LocalDateTime periodoFim = LocalDateTime.now();

        if (periodo.equals("Manhã")) {
            periodoInicio.withHour(06).withMinute(00).withSecond(0);
            periodoFim.withHour(12).withMinute(00).withSecond(0);
        } else if (periodo.equals("Tarde")) {
            periodoInicio.withHour(12).withMinute(00).withSecond(0);
            periodoFim.withHour(18).withMinute(00).withSecond(0);
        } else if (periodo.equals("Noite")) {
            periodoInicio.withHour(18).withMinute(00).withSecond(0);
            periodoFim.withHour(23).withMinute(59).withSecond(0);
        }

        System.out.println("***** Exibindo relatório do período atual ******");

        int i = 0;
        float totalRecebimento = 0;
        for (var movimentacao : estacionamento.RelatorioEntrada) {
            if (movimentacao.Data.getHour() > periodoInicio.getHour()
                    && movimentacao.Data.getHour() < periodoFim.getHour() && movimentacao.Tipo == tipo) {
                i++;
                totalRecebimento = movimentacao.Valor;

                System.out.println("N° " + i + "Data: " + movimentacao.Data + "Veículo: " + movimentacao.Veiculo.Modelo
                        + " - " + movimentacao.Veiculo.Cor + "Placa: " + movimentacao.Veiculo.Placa);

                System.out.println("O total de recebimentos nesse período foi de: " + totalRecebimento);
            }
        }

    }
}
