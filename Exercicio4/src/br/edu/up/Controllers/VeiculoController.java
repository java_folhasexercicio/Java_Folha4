package br.edu.up.Controllers;

import java.time.LocalDateTime;

import br.edu.up.Models.Veiculo;

public class VeiculoController {

    Veiculo veiculo = new Veiculo();

    public Veiculo CadastrarVeiculo(String modelo, String placa, String cor) throws Exception {
        try {
            if (modelo != "" && placa != "" && cor != "") {
                return veiculo.NovoVeiculo(modelo, placa, cor); // Na view tenho uma variavel que recebe esse retorno,
                                                                // mostrando carro e na qual vou manipular os registros
            } else {
                throw new Exception("Para registrar uma SAÍDA, um veículo deve ser informado");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

}
