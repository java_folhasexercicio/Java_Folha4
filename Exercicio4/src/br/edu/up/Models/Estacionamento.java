package br.edu.up.Models;

import java.util.ArrayList;
import java.util.List;

import br.edu.up.Models.Movimentacao.Tipo;

import java.time.LocalDateTime;

public class Estacionamento {

    public List<Veiculo> veiculos = new ArrayList<Veiculo>();
    public List<Movimentacao> RelatorioEntrada = new ArrayList<Movimentacao>();
    private static int QtdVagas = 10;

    public void NovaEntrada(Veiculo veiculo, LocalDateTime data) throws Exception {

        try {
            this.veiculos.add(veiculo);
            this.RemoveQtdVagas(1);
            this.RegistraMovimentacaoEntrada(veiculo, data);

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void NovaSaida(Veiculo veiculo, LocalDateTime data) throws Exception {

        try {

            this.veiculos.add(veiculo);
            this.RegistraMovimentacaoSaida(veiculo, data);
            this.AumentaQtdVagas(1);

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Boolean ConsultaPlaca(String Placa) throws Exception {

        try {
            for (var veiculo : this.veiculos) {
                if (veiculo.Placa == Placa)
                    ;
                return true;
            }
            return false;

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Boolean RegistraMovimentacaoEntrada(Veiculo veiculo, LocalDateTime data) throws Exception {
        try {
            if (veiculo != null || data != null) {

                RelatorioEntrada.add(new Movimentacao(veiculo, data, Tipo.Entrada, 5));
                return true;

            } else {
                throw new Exception("Para registrar uma movimentação, um veículo e uma data devem ser informados");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public Boolean RegistraMovimentacaoSaida(Veiculo veiculo, LocalDateTime data) throws Exception {
        try {
            if (veiculo != null || data != null) {

                RelatorioEntrada.add(new Movimentacao(veiculo, data, Tipo.Saida, 0));
                return true;

            } else {
                throw new Exception("Para registrar uma movimentação, um veículo e uma data devem ser informados");
            }

        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public void AumentaQtdVagas(int num) {
        this.QtdVagas += 1;
    }

    public void RemoveQtdVagas(int num) {
        this.QtdVagas -= 1;
    }

    public int ConsultaQtdVagas()
    {
        return this.QtdVagas;
    }
}
