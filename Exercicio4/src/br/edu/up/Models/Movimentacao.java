package br.edu.up.Models;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;

public class Movimentacao {

    public Veiculo Veiculo;
    public LocalDateTime Data;
    public Tipo Tipo;
    public Float Valor;

    public enum Tipo {
        Entrada, Saida
    }

    public Movimentacao(Veiculo veiculo, LocalDateTime data, Tipo tipo, float valor) {
        this.Veiculo = veiculo;
        this.Data = data;
        this.Tipo = tipo;
        this.Valor = valor;
    }

}
