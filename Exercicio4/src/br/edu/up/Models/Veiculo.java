package br.edu.up.Models;

public class Veiculo {

    public String Modelo;
    public String Placa;
    public String Cor;

    public Veiculo NovoVeiculo(String modelo, String placa, String cor) {
        this.Modelo = modelo;
        this.Placa = placa;
        this.Cor = cor;
        return this;
    }

}
