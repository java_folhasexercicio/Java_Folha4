package br.edu.up.Views;

import br.edu.up.Controllers.*;
import br.edu.up.Models.Movimentacao;
import br.edu.up.Models.Movimentacao.Tipo;

public class TelaPrincipal {

    public static void main(String[] args) {

        ControleController controle = new ControleController();
        VeiculoController veiculo = new VeiculoController();
        Prompt prompt = new Prompt();
        Boolean continuar = true;

        while (continuar) {

            var opcao = prompt.menuIniciar();

            if (opcao == 1) {
                prompt.imprimir("***** PRIMEIRAMENTE DEVEMOS CADASTRAR UM VEÍCULO *****");
                prompt.imprimir("1°) Infome o modelo do veículo: ");
                var modelo = prompt.lerLinha();

                prompt.imprimir("2°) Infome a placa do veículo: ");
                var placa = prompt.lerLinha();

                prompt.imprimir("3°) Infome a cor do veículo: ");
                var cor = prompt.lerLinha();

                try {
                    var carro = veiculo.CadastrarVeiculo(modelo, placa, cor);
                    if (carro != null) {
                        prompt.imprimir("Carro " + modelo + " - " + cor + " Placa: " + placa + "");
                    }

                    prompt.linhaEmBranco();

                    prompt.imprimir("Deseja registrar uma entrada? (Sim/Não)");
                    var decisao = prompt.lerLinha();
                    if (decisao.equals("Sim")) {
                        var entrada = controle.RegistraEntrada(carro);

                        if (entrada == true) {
                            prompt.imprimir("Entrada registrada com sucesso!");
                        } else {
                            throw new Exception("Falha ao registrar entrada. Tente novamente mais tarde");
                        }

                    } else {
                        prompt.imprimir("Finalizando programa...");
                        prompt.pressionarEnter();
                    }

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                }
            } else if (opcao == 2) {
                prompt.imprimir("Informe o período desejado para o relatório: (Manhã, Tarde ou Noite)");
                var periodo = prompt.lerLinha();

                prompt.imprimir("Informe o tipo de movimentação. (0 => ENTRADAS / 1 => SAÍDAS)");
                var tipo = prompt.lerInteiro();

                if (tipo == 0) {
                    controle.RelatorioPorPeriodoETipo(periodo, Tipo.Entrada);
                } else if (tipo == 1) {
                    controle.RelatorioPorPeriodoETipo(periodo, Tipo.Saida);
                } else {
                    prompt.imprimir("Opção Inválida!");
                }

            } else {
                prompt.imprimir("Opção Inválida!");
            }

            prompt.imprimir("Voltar ao Menu? (Sim/Não)");
            var decisaoContinuar = prompt.lerLinha();

            if (decisaoContinuar.equals("Sim")) {
                continuar = true;
            } else {
                continuar = false;
            }
        }
    }

}
