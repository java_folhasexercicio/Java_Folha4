package br.edu.up.View;
import java.util.Scanner;

public class menuView {
    private Scanner scanner;

    public menuView() {
        scanner = new Scanner(System.in);
    }

    public int exibirMenu() {
        System.out.println("===== Menu =====");
        System.out.println("1. Adicionar Evento");
        System.out.println("2. Listar Eventos");
        System.out.println("3. Adicionar Reserva");
        System.out.println("4. Listar Reservas");
        System.out.println("5. Sair");
        System.out.print("Escolha uma opção: ");
        return scanner.nextInt();
    }

    public void exibirMensagem(String mensagem) {
        System.out.println(mensagem);
    }
}

