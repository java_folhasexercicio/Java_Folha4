package br.edu.up.controller;

import java.util.ArrayList;
import java.util.List;
import br.edu.up.modelos.*;
import br.edu.up.View.*;


public class eventoController {
    private List<Evento> eventos;
    private menuView menuView;

    public eventoController(menuView menuView) {
        this.eventos = new ArrayList<>();
        this.menuView = menuView;
    }

    public void adicionarEvento(Evento evento) {
        eventos.add(evento);
        menuView.exibirMensagem("Evento adicionado com sucesso!");
    }

    public void listarEventos() {
        for (Evento evento : eventos) {
            System.out.println(evento);
        }
    }
}