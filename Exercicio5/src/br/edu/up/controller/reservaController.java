package br.edu.up.controller;

import java.util.ArrayList;
import java.util.List;
import br.edu.up.modelos.*;
import br.edu.up.View.*;
    
    public class reservaController {
        private List<Reserva> reservas;
        private menuView MenuView;
    
        public reservaController(MenuView menuView) {
            this.reservas = new ArrayList<>();
            this.MenuView = MenuView;
        }
    
        public void adicionarReserva(Reserva reserva) {
            reservas.add(reserva);
            MenuView.exibirMensagem("Reserva realizada com sucesso!");
        }
    
        public void listarReservas() {
            for (Reserva reserva : reservas) {
                System.out.println(reserva);
            }
        }
    }