import java.time.LocalDate;
import java.util.List;

public class ControllerConsultaPassageiro {

    private TelaConsultaPassageiro telaConsultaPassageiro;
    private List<Passageiro> passageiros;

    public ControllerConsultaPassageiro(TelaConsultaPassageiro telaConsultaPassageiro, List<Passageiro> passageiros) {
        this.telaConsultaPassageiro = telaConsultaPassageiro;
        this.passageiros = passageiros;
    }

    public void consultarPassageiro() {
        telaConsultaPassageiro.consultarPassageiro(passageiros);
    }
}
