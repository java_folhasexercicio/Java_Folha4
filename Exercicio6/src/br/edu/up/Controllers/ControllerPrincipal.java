package br.edu.up.Controllers;

import java.util.List;

public class ControllerPrincipal {

    private TelaPrincipal telaPrincipal;
    private ControllerRegistroPassageiro controllerRegistroPassageiro;
    private ControllerConsultaPassageiro controllerConsultaPassageiro;

    // ... (outros controllers para registro e consulta de tripulantes e aeronaves)

    public ControllerPrincipal(TelaPrincipal telaPrincipal,
                                ControllerRegistroPassageiro controllerRegistroPassageiro,
                                ControllerConsultaPassageiro controllerConsultaPassageiro) {
        this.telaPrincipal = telaPrincipal;
        this.controllerRegistroPassageiro = controllerRegistroPassageiro;
        this.controllerConsultaPassageiro = controllerConsultaPassageiro;
    }

    public void iniciar() {
        int opcaoEscolhida;
        do {
            telaPrincipal.exibirMenu();
            opcaoEscolhida = telaPrincipal.getOpcaoEscolhida();

            switch (opcaoEscolhida) {
                case 1:
                    controllerRegistroPassageiro.registrarPassageiro();
                    break;
                case 2:
                    controllerConsultaPassageiro.consultarPassageiro();
                    break;
                // ... (casos para outras funcionalidades, como registro e consulta de tripulantes e aeronaves)
                case 7:
                    System.out.println("Saindo do sistema...");
                    break;
                default:
                    System.out.println("Opção inválida. Tente novamente.");
            }
        } while (opcaoEscolhida != 7);
    }
}
