package br.edu.up.Controllers;

import java.util.List;

public class ControllerRegistroPassageiro {

    private TelaRegistroPassageiro telaRegistroPassageiro;
    private List<Passageiro> passageiros;

    // ... (injeção de dependência de outros serviços, como serviço de registro de passageiro)

    public ControllerRegistroPassageiro(TelaRegistroPassageiro telaRegistroPassageiro, List<Passageiro> passageiros) {
        this.telaRegistroPassageiro = telaRegistroPassageiro;
        this.passageiros = passageiros;
    }

    public void registrarPassageiro() {
        String dadosPassageiro = telaRegistroPassageiro.obterDadosPassageiro();
        // ... (converter dados do passageiro para objeto Passageiro)
        passageiros.add(passageiro);
        System.out.println("Passageiro registrado com sucesso!");
    }
}
