package br.edu.up.Models;

public class Comandante extends Tripulante {
    private int totalHorasVoo;

    public Comandante(String nome, String rg, String matriculaFuncionario,
                       String identificacaoAeronautica, int totalHorasVoo) {
        super(nome, rg, matriculaFuncionario, identificacaoAeronautica);
        this.totalHorasVoo = totalHorasVoo;
    }

    public int getTotalHorasVoo() {
        return totalHorasVoo;
    }
}

