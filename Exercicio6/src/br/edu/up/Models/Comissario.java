package br.edu.up.Models;

public class Comissario extends Tripulante {
    private List<String> idiomasFluencia;

    public Comissario(String nome, String rg, String matriculaFuncionario,
                       String identificacaoAeronautica, List<String> idiomasFluencia) {
        super(nome, rg, matriculaFuncionario, identificacaoAeronautica);
        this.idiomasFluencia = idiomasFluencia;
    }

    public List<String> getIdiomasFluencia() {
        return idiomasFluencia;
    }
}

