package br.edu.up.Models;

public class Passageiro extends Pessoa {
    private String idBagagem;
    private String numeroAssento;
    private String classeAssento;
    private Voo voo;

    public Passageiro(String nome, String rg, String idBagagem, String numeroAssento,
                       String classeAssento, Voo voo) {
        super(nome, rg);
        this.idBagagem = idBagagem;
        this.numeroAssento = numeroAssento;
        this.classeAssento = classeAssento;
        this.voo = voo;
    }

    public String getIdBagagem() {
        return idBagagem;
    }

    public String getNumeroAssento() {
        return numeroAssento;
    }

    public String getClasseAssento() {
        return classeAssento;
    }

    public Voo getVoo() {
        return voo;
    }
}
