package br.edu.up.Models;

public class Pessoa implements IPessoa {
    private String nome;
    private String rg;

    public Pessoa(String nome, String rg) {
        this.nome = nome;
        this.rg = rg;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public String getRg() {
        return rg;
    }
}

 