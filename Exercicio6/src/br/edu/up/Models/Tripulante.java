package br.edu.up.Models;

public class Tripulante extends Pessoa {
    private String matriculaFuncionario;
    private String identificacaoAeronautica;

    public Tripulante(String nome, String rg, String matriculaFuncionario,
                       String identificacaoAeronautica) {
        super(nome, rg);
        this.matriculaFuncionario = matriculaFuncionario;
        this.identificacaoAeronautica = identificacaoAeronautica;
    }

    public String getMatriculaFuncionario() {
        return matriculaFuncionario;
    }

    public String getIdentificacaoAeronautica() {
        return identificacaoAeronautica;
    }
}

