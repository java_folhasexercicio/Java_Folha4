import java.util.Scanner;

public class TelaPrincipal {

    private ControllerPrincipal controllerPrincipal;

    public TelaPrincipal(ControllerPrincipal controllerPrincipal) {
        this.controllerPrincipal = controllerPrincipal;
    }

    public void exibirMenu() {
        Prompt prompt = new Prompt(); 
        prompt.separador();
        prompt.imprimir("Controle de Tráfego Aéreo");
        prompt.separador();
        prompt.imprimir("1. Registrar Passageiro");
        prompt.imprimir("2. Consultar Passageiro");
        prompt.imprimir("3. Registrar Tripulante");
        prompt.imprimir("4. Consultar Tripulante");
        prompt.imprimir("5. Registrar Aeronave");
        prompt.imprimir("6. Consultar Aeronave");
        prompt.imprimir("7. Sair");

        prompt.imprimir("Digite a opção desejada: ");
    }

    public int getOpcaoEscolhida() {
        Prompt prompt = new Prompt();
        return prompt.lerInteiro();
    }
}
