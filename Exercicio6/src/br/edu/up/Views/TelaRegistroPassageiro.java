import java.time.LocalDate;

public class TelaRegistroPassageiro {

    private Prompt prompt;

    public TelaRegistroPassageiro(Prompt prompt) {
        this.prompt = prompt;
    }

    public String obterDadosPassageiro() {
        prompt.separador();
        prompt.imprimir("Registro de Passageiro");
        prompt.separador();

        String nome = prompt.lerLinha("Nome: ");
        String rg = prompt.lerLinha("RG: ");
        String idBagagem = prompt.lerLinha("Identificador de Bagagem: ");
        String numeroAssento = prompt.lerLinha("Número do Assento: ");
        String classeAssento = prompt.lerLinha("Classe do Assento (Econômica, Executiva, etc.): ");

        LocalDate dataVoo = obterDataVoo("Data do Voo (dd/MM/yyyy): ");
        int hora = prompt.lerInteiro("Hora do Voo (0-23): ");
        int minuto = prompt.lerInteiro("Minuto do Voo (0-59): ");
        LocalTime horaVoo = LocalTime.of(hora, minuto);

        return nome + "," + rg + "," + idBagagem + "," + numeroAssento + "," 
               + classeAssento + "," + dataVoo.getDayOfMonth() + "," + dataVoo.getMonthValue() + "," 
               + dataVoo.getYear() + "," + horaVoo.getHour() + "," + horaVoo.getMinute();
    }

    private LocalDate obterDataVoo(String mensagem) {
        while (true) {
            try {
                String dataString = prompt.lerLinha(mensagem);
                return LocalDate.parse(dataString, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            } catch (DateTimeParseException e) {
                System.out.println("Data inválida, digite no formato dd/MM/yyyy: ");
            }
        }
    }
}
