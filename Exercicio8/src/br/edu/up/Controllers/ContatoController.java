package br.edu.up.Controllers;

import br.edu.up.Models.*;

public class ContatoController {
    private Agenda agenda;

    public ContatoController() {
        this.agenda = new Agenda();
    }

    public void incluirContatoPessoal(String nome, String telefone, int codigo, String aniversario) {
        Pessoal contato = new Pessoal(nome, telefone, codigo, aniversario);
        agenda.adicionar(contato);
    }

    public void incluirContatoComercial(String nome, String telefone, int codigo, String cnpj) {
        Comercial contato = new Comercial(nome, telefone, codigo, cnpj);
        agenda.adicionar(contato);
    }

    public void excluirContato(int codigo) {
        agenda.excluirContato(codigo);
    }

    public Contato consultarContato(int codigo) {
        return agenda.getContato(codigo);
    }

    public String listarContatos() {
        return agenda.listarContatos();
    }
}

