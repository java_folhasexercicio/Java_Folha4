package br.edu.up.Models;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
    private List<Contato> contatos;

    public Agenda() {
        this.contatos = new ArrayList<>();
    }

    public void adicionar(Comercial contato) {
        contatos.add(contato);
    }

    public void adicionar(Pessoal contato) {
        contatos.add(contato);
    }

    public Contato getContato(int codigo) {
        for (Contato contato : contatos) {
            if (contato.getCodigo() == codigo) {
                return contato;
            }
        }
        return null; 
    }

    public String listarContatos() {
        String lista = "";
        for (Contato contato : contatos) {
            lista += contato + "\n";
        }
        return lista;
    }
    

}
