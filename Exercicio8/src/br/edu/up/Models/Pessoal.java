package br.edu.up.Models;

public class Pessoal extends Contato {
    private String aniversario;

    public Pessoal(String nome, String telefone, int codigo, String aniversario) {
        super(codigo, nome, telefone);
        this.aniversario = aniversario;
    }

    public String getAniversario() {
        return aniversario;
    }

    public void setAniversario(String aniversario) {
        this.aniversario = aniversario;
    }

    @Override
    public String toString() {
        return super.toString() + ", Aniversário: " + aniversario;
    }
}
