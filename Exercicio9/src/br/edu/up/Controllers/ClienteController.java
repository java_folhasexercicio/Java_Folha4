package br.edu.up.Controllers;

package br.edu.up.Controllers;

import br.edu.up.Models.*;


import java.util.ArrayList;
import java.util.List;

public class ClienteController {
    private List<Cliente> clientes;

    public ClienteController() {
        this.clientes = new ArrayList<>();
    }

    public void incluirClientePessoa(String nome, String telefone, String email, double vlrMaxCredito, Endereco endereco, String cpf, double peso, double altura) {
        ClientePessoa clientePessoa = new ClientePessoa(nome, telefone, email, vlrMaxCredito, endereco, cpf, peso, altura);
        clientes.add(clientePessoa);
    }

    public void incluirClienteEmpresa(String nome, String telefone, String email, double vlrMaxCredito, Endereco endereco, String cnpj, String inscricaoEstadual, int anoFundacao) {
        ClienteEmpresa clienteEmpresa = new ClienteEmpresa(nome, telefone, email, vlrMaxCredito, endereco, cnpj, inscricaoEstadual, anoFundacao);
        clientes.add(clienteEmpresa);
    }

    public void mostrarDadosClientePessoa(int indice) {
        if (indice >= 0 && indice < clientes.size()) {
            Cliente cliente = clientes.get(indice);
            if (cliente instanceof ClientePessoa) {
                ClientePessoa clientePessoa = (ClientePessoa) cliente;
                System.out.println(clientePessoa.getDados());
            } else {
                System.out.println("O cliente não é uma pessoa.");
            }
        } else {
            System.out.println("Índice inválido.");
        }
    }

    public void mostrarDadosClienteEmpresa(int indice) {
        if (indice >= 0 && indice < clientes.size()) {
            Cliente cliente = clientes.get(indice);
            if (cliente instanceof ClienteEmpresa) {
                ClienteEmpresa clienteEmpresa = (ClienteEmpresa) cliente;
                System.out.println(clienteEmpresa.getDados());
            } else {
                System.out.println("O cliente não é uma empresa.");
            }
        } else {
            System.out.println("Índice inválido.");
        }
    }
}

