package br.edu.up.Controllers;

import br.edu.up.Models.Cliente;
import br.edu.up.Models.ClienteEmpresa;
import br.edu.up.Models.ClientePessoa;

import java.util.ArrayList;
import java.util.List;

public class OperacoesController {
    private List<ClientePessoa> clientesPessoas;
    private List<ClienteEmpresa> clienteEmpresas;

    public OperacoesController() {
        this.clientesPessoas = new ArrayList<>();
        this.clienteEmpresas = new ArrayList<>();
    }

    public void adicionarClientePessoa(ClientePessoa clientePessoa) {
        clientesPessoas.add(clientePessoa);
    }

    public void adicionarClienteEmpresa(ClienteEmpresa clienteEmpresa) {
        clienteEmpresas.add(clienteEmpresa);
    }

    public String emprestarClientePessoa(int indice, double valor) {
        if (indice >= 0 && indice < clientesPessoas.size()) {
            return clientesPessoas.get(indice).emprestar(valor);
        } else {
            return "Índice de cliente pessoa inválido.";
        }
    }

    public String emprestarClienteEmpresa(int indice, double valor) {
        if (indice >= 0 && indice < clienteEmpresas.size()) {
            return clienteEmpresas.get(indice).emprestar(valor);
        } else {
            return "Índice de cliente empresa inválido.";
        }
    }

    public double devolverClientePessoa(int indice, double valor) {
        if (indice >= 0 && indice < clientesPessoas.size()) {
            return clientesPessoas.get(indice).devolver(valor);
        } else {
            return 0;
        }
    }

    public double devolverClienteEmpresa(int indice, double valor) {
        if (indice >= 0 && indice < clienteEmpresas.size()) {
            return clienteEmpresas.get(indice).devolver(valor);
        } else {
            return 0;
        }
    }
}
