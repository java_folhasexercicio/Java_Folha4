package br.edu.up.Models;

public class Cliente {

    private String nome;
    private String telefone;
    private String email;
    private double vlrMaxCredito;
    private double vlrEmprestado;
    private Endereco endereco;

    public Cliente(String nome, String telefone, String email, double vlrMaxCredito, Endereco endereco) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.vlrMaxCredito = vlrMaxCredito;
        this.vlrEmprestado = 0;
        this.endereco = endereco;
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public double getVlrMaxCredito() {
        return vlrMaxCredito;
    }

    public double getVlrEmprestado() {
        return vlrEmprestado;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setVlrMaxCredito(double vlrMaxCredito) {
        this.vlrMaxCredito = vlrMaxCredito;
    }

    public String emprestar(double valor) {
        if (valor <= (vlrMaxCredito - vlrEmprestado)) {
            vlrEmprestado += valor;
            return "Empréstimo de R$" + valor + " realizado com sucesso!";
        } else {
            return "Limite de crédito insuficiente. Valor máximo disponível para empréstimo: R$" + (vlrMaxCredito - vlrEmprestado);
        }
    }

    public double devolver(double valor) {
        if (valor <= vlrEmprestado) {
            vlrEmprestado -= valor;
            return valor;
        } else {
            return 0;
        }
    }

    public double getSaldo() {
        return vlrMaxCredito - vlrEmprestado;
    }
}
