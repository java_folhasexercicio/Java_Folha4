package br.edu.up.Models;

public class ClienteEmpresa extends Cliente {

    private String cnpj;
    private String inscricaoEstadual;
    private int anoFundacao;

    public ClienteEmpresa(String nome, String telefone, String email, double vlrMaxCredito, Endereco endereco, String cnpj, String inscricaoEstadual, int anoFundacao) {
        super(nome, telefone, email, vlrMaxCredito, endereco); // Chamada ao construtor da superclasse
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.anoFundacao = anoFundacao;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public int getAnoFundacao() {
        return anoFundacao;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public void setAnoFundacao(int anoFundacao) {
        this.anoFundacao = anoFundacao;
    }

    @Override
    public String toString() {
        return "ClienteEmpresa{" +
                "nome='" + getNome() + '\'' +
                ", telefone='" + getTelefone() + '\'' +
                ", email='" + getEmail() + '\'' +
                ", vlrMaxCredito=" + getVlrMaxCredito() +
                ", endereco=" + getEndereco() +
                ", cnpj='" + cnpj + '\'' +
                ", inscricaoEstadual='" + inscricaoEstadual + '\'' +
                ", anoFundacao=" + anoFundacao +
                '}';
    }

    @Override
    public double getSaldo() {
        return super.getSaldo();
    }

    public String getDados() {
        return "Dados da Empresa:\n" +
                "Nome: " + getNome() + "\n" +
                "CNPJ: " + cnpj + "\n" +
                "Inscrição Estadual: " + inscricaoEstadual + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Email: " + getEmail() + "\n" +
                "Endereço: " + getEndereco() + "\n" +
                "Limite de Crédito: R$" + getVlrMaxCredito() + "\n" +
                "Saldo Disponível: R$" + getSaldo() + "\n" +
                "Ano de Fundação: " + anoFundacao;
    }
}

