package br.edu.up.Models;

public class ClientePessoa extends Cliente {

    private String cpf;
    private double peso;
    private double altura;


    public ClientePessoa(String nome, String telefone, String email, double vlrMaxCredito, Endereco endereco, String cpf, double peso, double altura) {
        super(nome, telefone, email, vlrMaxCredito, endereco); 
        this.peso = peso;
        this.altura = altura;
    }

    public String getCpf() {
        return cpf;
    }

    public double getPeso() {
        return peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

  
    @Override
    public String toString() {
        return "ClientePessoa{" +
                "nome='" + getNome() + '\'' +
                ", telefone='" + getTelefone() + '\'' +
                ", email='" + getEmail() + '\'' +
                ", vlrMaxCredito=" + getVlrMaxCredito() +
                ", endereco=" + getEndereco() +
                ", cpf='" + cpf + '\'' +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }


    @Override
    public double getSaldo() {
        return super.getSaldo();
    }

 
    public String getDados() {
        return "Dados do Cliente:\n" +
                "Nome: " + getNome() + "\n" +
                "CPF: " + cpf + "\n" +
                "Telefone: " + getTelefone() + "\n" +
                "Email: " + getEmail() + "\n" +
                "Endereço: " + getEndereco() + "\n" +
                "Limite de Crédito: R$" + getVlrMaxCredito() + "\n" +
                "Saldo Disponível: R$" + getSaldo() + "\n" +
                "Peso: " + peso + " kg\n" +
                "Altura: " + altura + " m";
    }
}
