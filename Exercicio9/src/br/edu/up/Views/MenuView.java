package br.edu.up.Views;

import java.util.Scanner;

public class MenuView {
    private ClienteView clienteView;
    private OperacaoView operacaoView;
    private Scanner scanner;

    public MenuView() {
        this.clienteView = new ClienteView();
        this.operacaoView = new OperacaoView();
        this.scanner = new Scanner(System.in);
    }

    public void menuPrincipal() {
        int opcao;
        do {
            System.out.println("===== Menu Principal =====");
            System.out.println("1. Incluir Cliente Pessoa");
            System.out.println("2. Incluir Cliente Empresa");
            System.out.println("3. Mostrar Dados Cliente Pessoa");
            System.out.println("4. Mostrar Dados Cliente Empresa");
            System.out.println("5. Empréstimo para Cliente Pessoa");
            System.out.println("6. Empréstimo para Cliente Empresa");
            System.out.println("7. Devolução de Cliente Pessoa");
            System.out.println("8. Devolução de Cliente Empresa");
            System.out.println("9. Sair");
            System.out.print("Escolha uma opção: ");
            opcao = scanner.nextInt();

            switch (opcao) {
                case 1:
                    clienteView.incluirClientePessoa();
                    break;
                case 2:
                    clienteView.incluirClienteEmpresa();
                    break;
                case 3:
                    clienteView.mostrarDadosClientePessoa();
                    break;
                case 4:
                    clienteView.mostrarDadosClienteEmpresa();
                    break;
                case 5:
                    operacaoView.operacaemprestarClientePessoa();
                    break;
                case 6:
                    operacaoView.emprestarClienteEmpresa();
                    break;
                case 7:
                    operacaoView.devolverClientePessoa();
                    break;
                case 8:
                    operacaoView.devolverClienteEmpresa();
                    break;
                case 9:
                    System.out.println("Saindo...");
                    break;
                default:
                    System.out.println("Opção inválida.");
            }
        } while (opcao != 9);
    }
}
