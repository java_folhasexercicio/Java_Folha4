package br.edu.up.Views;

package br.edu.up.Views;

import br.edu.up.Controllers.OperacoesController;

import java.util.Scanner;

public class OperacaoView {
    private OperacoesController operacoesController;
    private Scanner scanner;

    public OperacaoView() {
        this.operacoesController = new OperacoesController();
        this.scanner = new Scanner(System.in);
    }
    private void emprestarClientePessoa() {
        System.out.println("===== Empréstimo para Cliente Pessoa =====");
        System.out.print("Índice do cliente pessoa: ");
        int indice = scanner.nextInt();
        System.out.print("Valor do empréstimo: ");
        double valor = scanner.nextDouble();

        String mensagem = operacoesController.emprestarClientePessoa(indice, valor);
        System.out.println(mensagem);
    }

    private void emprestarClienteEmpresa() {
        System.out.println("===== Empréstimo para Cliente Empresa =====");
        System.out.print("Índice do cliente empresa: ");
        int indice = scanner.nextInt();
        System.out.print("Valor do empréstimo: ");
        double valor = scanner.nextDouble();

        String mensagem = operacoesController.emprestarClienteEmpresa(indice, valor);
        System.out.println(mensagem);
    }

    private void devolverClientePessoa() {
        System.out.println("===== Devolução de Cliente Pessoa =====");
        System.out.print("Índice do cliente pessoa: ");
        int indice = scanner.nextInt();
        System.out.print("Valor da devolução: ");
        double valor = scanner.nextDouble();

        double valorDevolvido = operacoesController.devolverClientePessoa(indice, valor);
        System.out.println("Valor devolvido: " + valorDevolvido);
    }

    private void devolverClienteEmpresa() {
        System.out.println("===== Devolução de Cliente Empresa =====");
        System.out.print("Índice do cliente empresa: ");
        int indice = scanner.nextInt();
        System.out.print("Valor da devolução: ");
        double valor = scanner.nextDouble();

        double valorDevolvido = operacoesController.devolverClienteEmpresa(indice, valor);
        System.out.println("Valor devolvido: " + valorDevolvido);
    }
}
